
# coding: utf-8

# #Scrape 102 - Scrape Company Information from Linkedin Public Pages

# In this example, we scrape top business school information from usnews 
# 
# For example:  http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/carlson-school-of-management-01126
# 
# We primarily rely on the document's structure to locate the content that we are interested in.
# 
# We also save the results in a dict and store them in a json file. 
# 
# We then scale it up to scrape a list of schools, but sleep between requests to be polite.
# 
# What we want to produce is a list of dicts like this:
# 
# ```
# {
#     "total program (executive degree, out-of-state)": 108000.0, 
#     "url": "university-of-minnesota-twin-cities-carlson-01126", 
#     "total program (executive degree, in-state)": 108000.0, 
#     "per credit (part-time, in-state)": 1270.0, 
#     "per year (full-time, in-state)": 35260.0, 
#     "enrolled (part-time)": 1122.0, 
#     "per credit (part-time, out-of-state)": 1270.0, 
#     "enrolled (full-time)": 220.0, 
#     "per year (full-time, out-of-state)": 45000.0
# }
# ```
# 
# 

# #Retrieve and parse a single page
# ##todo:
# Observe and run the next four cells. Make sure you are getting a proper soupified html page

# In[2]:

from bs4 import BeautifulSoup
from pylib import utils
from pprint import pprint #used for pretty printing of python list/dict objects
import requests


# In[3]:

# request and get 
url = "university-of-minnesota-twin-cities-carlson-01126"
response = requests.get("http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/"+url)
html = response.text


# In[4]:

print response.status_code #200 represents ok


# In[5]:

#soupify it first.
soup = BeautifulSoup(html)


# ## todo:
# Inspect the page http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/carlson-school-of-management-01126 using Chrome. Can you locate the tags containing school address, students, and tuition information? What's your strategy of getting them?

# ##todo:
# find the ```<div>``` tag containing the above info (hint: using class ```quick-stats```). Then save it in a soup object ```node```

# In[6]:

# find the information block first
# save the find to 'node'
node = soup.find('div',class_="quick-stats")


# ##todo:
# Observe and run the next two cells to see the strategy of locating the right info
# 
# 
# Pay attention to error checking to avoid pitfalls (e.g. when a tag was not found)

# In[11]:

data = {} #define a dict to hold data.

# we use two level tags to location the location block
# the most convenient way is to use css-style selector
# here we look for a div with class p, followed by a tag p underneath.
# select function returns a list or []
location = node.select('div.icon-location p')

# it is important check whether we have found anything
if location:
    # we then extract the address
    data['location']=location[0].get_text().strip()

#use a similar strategy to find student enrollments, 
# but there could be multiple finds
for person in node.select('div.icon-person p'):
    #find function locates the first span - this is the number
    number = person.find("span")
    if number is not None:
        # the next tag is the label
        label = number.find_next()
        if label is not None:
            #utils.parse_decimal is one of the convenience functions to extra decimals
            #it deals with comma ',' and '$' etc
            data[label.string.strip()]=utils.parse_decimal(number.string.strip())


# In[12]:

pprint(data)


# ##todo:
# Now it is your turn to find tuition related numbers and save them to ```data``` as well.
# 
# Again, we use the labels as dictionary keys and numbers as values.
# 
# Then, test your script in the next cell

# In[13]:

# extract and save tuition dollars here
for person in node.select('div.icon-dollar p'):
    number = person.find("span")
    if number is not None:
        label = number.find_next()
        if label is not None:
            data[label.string.strip()]=utils.parse_decimal(number.string.strip())


# In[14]:

pprint(data)


# #2. Scale up so that we may do multiple schools
# Now we will reorganize the previous functions into two functions
# - The first function is responsible for retrieving the page
# - The next one is for parsing the page
# 
# ## todo:
# Now observe the first function

# In[15]:

def fetch_school(school_url):
    ''' fetch a page and return raw html
    Args:
        school_url: page url, e.g. northwestern-university-kellogg-01071
    Returns:
        html: raw html 
    '''
    print "fetching %s..."%school_url
    url = "http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/{}".format(school_url)
    response = requests.get(url)
    html = response.text

    return html


# ##todo:
# Now we ask you to put together the next function. 
# 
# Then test it with the next two cells

# In[16]:

def parse_school(html, school_url):
    ''' extract school information from html and return a dict
    
    in addition to address, enrollment, and tuition info, we also save 
    school_url information in the dict
    
    Args: 
        html: raw html source
        school_url: url of the school
    
    Returns:
        data: a dict contains school profile info
    '''
    
    soup = BeautifulSoup(html)
    
    data = {"url":school_url}
    
    node = soup.find('div',class_="quick-stats")
    location = node.select('div.icon-location p')
    
    if not location:
        data['location']=location[0].string.strip()

    for person in node.select('div.icon-person p'):
        number = person.find("span")
        if number is not None:
            label = number.find_next()
            if label is not None:
                data[label.string.strip()]=utils.parse_decimal(number.string.strip())

    for person in node.select('div.icon-dollar p'):
        number = person.find("span")
        if number is not None:
            label = number.find_next()
            if label is not None:
                data[label.string.strip()]=utils.parse_decimal(number.string.strip())
    
    return data


# In[17]:

# test the functions
school = "arizona-state-university-carey-01007"
html = fetch_school(school)


# In[18]:

pprint(parse_school(html, school))


# ##2. Now scale it up
# 

# ##todo:
# observe and run the following two cells 

# In[19]:

#list of company names
schools = ['mccombs-school-of-business-01222',
           'university-of-minnesota-twin-cities-carlson-01126', 
           'kenan-flagler-business-school-01165']


# In[1]:

#to save results in output
output = []
# Loop over the list of school urls
for school in schools:
    # Get the html data
    html = fetch_school(school)
    data = parse_school(html, school)
    output.append(data)
    # Sleep randomly 5-10 seconds to be nice
    utils.my_sleep()


# ##todo:
# Observe and run the following cell

# In[21]:

# the custom function dump_json opens a file and write json into it.
# it deals with unicode encoding etc.
utils.dump_json(output, 'data/results.json')


# # Reading school list from an external csv file

# #todo:
# Observe and run the following two cells to get a list of schools from a csv file

# In[22]:

import csv
with open('data/schoollist.csv', 'rb') as f:
    reader = csv.reader(f)
    schoollist = list(reader)


# In[23]:

#to print a sample
pprint(schoollist[0:5])


# #todo:
# Now it is your turn to crawl the school's pages using the schoolist
# 
# To save time, we limit to the first five schools.
# 
# After you are done, use the next cell to test your results

# In[24]:

#to save results
output = []
# Loop over the list of school urls
for (school,name,rank) in schoollist[0:5]:
    # Get the html data
    html = fetch_school(school)
    data = parse_school(html, school)
    output.append(data)
    # Sleep randomly 5-10 seconds to be nice
    utils.my_sleep()


# In[25]:

pprint(output)


# In[ ]:



