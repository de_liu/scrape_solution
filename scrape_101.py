
# coding: utf-8

# # Scrape 101 - Scrape US Universities
# 
# You may find US universities from this webpage. 
# http://www.utexas.edu/world/univ/alpha/
# 
# We will try to extract the university URLs as well as names.
# 
# We will use two packages
# 
# - ```requests```: for making http requests. see http://docs.python-requests.org/en/latest/ for details
# - ```bs4``` (beautifulsoup4): for parsing the content of the page and searching content within it, see http://www.crummy.com/software/BeautifulSoup/bs4/doc/# for official documentation.

# ## todo:
# 
# Observe the following four blocks of python code

# In[ ]:

from bs4 import BeautifulSoup
import requests


# In[ ]:

# make a request for a web page
response = requests.get("http://www.utexas.edu/world/univ/alpha/")


# In[ ]:

# .text contains response content as a string object  
html = response.text


# In[ ]:

#BeautifulSoup takes a string object and parse out the document structure and turn it into a BeautifulSoup object.
soup = BeautifulSoup(html)


# ## todo:
# 
# Now you should navigate to http://www.utexas.edu/world/univ/alpha/ using a Chrome browser.
# 
# Right click on a url, choose  "inspect element" to inspect HTML source.
# 
# How can we accurately locate universities (url and name)?

# In[ ]:

# now you can search within the BeautifulSoup object
# this is an example of searching for tags by class
for u in soup.find_all('a',class_='institution'):
    # we can extract entity attributes using ['attr_name']
    # and the text inside an entity using .string
    # note: encode("utf-8") is needed for console printing because unicode characters cannot be 
    # directly written to console output without proper encoding (the same goes for a file output).     
    print "%s, %s"%(u['href'],u.string.encode('utf-8'))


# ## todo: 
# 
# instead of printing the results, please replicate the above but save the results to a list of tuples
# 
# such as 
# ```
# [(url1, univ1), (url2, univ2), ... ]
# ```
# 
# It is a good time to recall python list comprehension, in the form of 
# ```
# [ list_element_constructed_using_x for x in list ]
# ```

# In[ ]:

# search and save the results to a list object called universities.
universities = [(u['href'],u.get_text()) for u in soup.find_all('a',class_='institution')]


# ## todo:
# print the first 10 universities in the form of 
# ```
# url1, univ1
# url2, univ2
# ```

# In[ ]:

# print first 10
for u in universities[0:10]:
    print "%s, %s"%u


# #todo:
# observe and then run the following code block
# 
# check out what ```dump_csv()``` does by inspecting ```pylib/utils.py``` if you are interested.
# 
# check out the resulting csv file

# In[ ]:

# believe it or not, writting to a properly formatted CSV is a nontrivial task 
# so I have written a function called dump_csv(data, filename) to take care of some of the details,
# such as encoding, proper line formats, etc.
# the function returns the number of rows written.

from pylib import utils

utils.dump_csv(universities,"universities.csv")


# ## todo:
# Now, as an exercise, get a list of states and their abbreviations from this page http://www.utexas.edu/world/univ/state/,
# then print the first 10 states the the form of:
# ```
# AL, Alabama
# AR, Arkansas
# CO, Colorado
# ....
# ```

# In[ ]:

# request the page
state_response = requests.get("http://www.utexas.edu/world/univ/state/")


# In[ ]:

# extract html content from the response
state_html = state_response.text


# In[ ]:

# soupify it, and save to state_soup 
state_soup = BeautifulSoup(state_html)


# In[ ]:

# parse out state abbrevations and states, save them to a list object named states
states = [(state['id'], state.string) for state in state_soup.find_all('h2')]


# In[ ]:

# print the first 10
for s in states[0:10]:
    print "%s, %s"%s


# ## todo (optional):
# As an advanced exercise *on your own*, can you get universities within states and save data in the form of
# ```
# Alabama, Air University, http://www.maxwell.af.mil/units/au/
# Alabama, Alabama A&M University, http://www.aamu.edu/
# ...
# ```
# Hint: the trick is to use the ```find_next()``` function in BeautifulSoup, which allows you to search near a given tag 

# In[ ]:

# parse a list of universities along with their states
universities = []
for s in state_soup.find_all('h2'):
    # we now have located the h2 tag corresponds to a state
    state = s.string
    # note that universities are listed underneath the div tag next to h2.
    # so we use a combination of find_next and find_all to search with in the next
    # div tag after h2.
    for u in s.find_next('div', class_="box2").find_all('a', class_='institution'):
        universities.append((state, u.get_text(), u['href']))


# In[ ]:

# print the first 10 universities
for u in universities[0:10]:
    print "%s, %s, %s"%u


# In[ ]:



