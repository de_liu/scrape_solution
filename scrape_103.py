
# coding: utf-8

# #Scrape 103 - Get a List of Schools from US News 

# This example shows how we deal with pagination, such as the example here:
# 
# http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/mba-rankings
# 
# This example produces a list of schools (URLs) which then will be used with the page-by-page crawler introduced in Scrape102. Thus we have a full cycle.  
# 
# The output of this program is a csv file in the form of school url, school name, and ranking, e.g.: 
# ```
# graduate-school-of-business-01028,Stanford University,1.0
# business-school-01110,Harvard University,2.0
# wharton-school-01194,University of Pennsylvania (Wharton),3.0
# booth-school-of-business-01073,University of Chicago (Booth),4.0
# ```
# 
# 

# In[ ]:

from bs4 import BeautifulSoup
from pylib import utils
import requests
from pprint import pprint


# ##todo:
# Observe the page http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/mba-rankings/page+2. Pause on the pagination area to see a paginated URL. 
# 
# You may modify the url in your browser to test your ideas about paginated url.
# 

# ## We first build functions to handle one page

# ##todo:
# Observe the following function and complete it as instructed. Test it in the next cell

# In[ ]:

def fetch_page(page):
    ''' fetch one page among several pages
    Args: 
        page: which page to fetch?
    Returns:
        html: html source of the retrieved page
    '''
    url = "http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/mba-rankings"
    if page > 1:
        ## todo: Please fill in how you would modify the url to incorporate pagination here:
        url = url + "/page+"+str(page)
    
    print("fetching page %s"%page)
        
    html = requests.get(url).text  
       
    return html


# In[ ]:

# test the fetch_page function
page2 = fetch_page(2)
print page2[0:2000]


# ##todo:
# Observe the following function, then fill the blank as instructed.
# 
# Test your function in the next cell

# In[ ]:

def parse_page(html):
    ''' parse a list of schools from one html page
    
    Args:
        html: html source
    Returns:
        data: a list of schools
    '''
    #soupify
    soup = BeautifulSoup(html)

    num_found = 0
    # to save a list of schools
    schools = []    

    for tr in soup.select('table.ranking-data tr[valign]'):
        num_found += 1

        data = {} #create a new dict. It is important to know that we need to do this for each school

        rank= tr.find("span",class_="rankscore-bronze")
        if rank is not None:
            #note that when a tag contains multiple things, .string is confused and returns nothing.
            #but get_text will give you pure text within a tag, no matter how many other things are there
            data['rank'] = utils.parse_decimal(rank.get_text())
        else:
            print "cannot locate rank score"
            print tr
            quit()

        ##todo: please extract school name and URL, save them in data['href'] and data['name'] respectively.
        ## an example of data['href'] is "graduate-school-of-business-01028"
        ##note that you may use utils.substr_after(text, search_pattern) to get text after the search pattern
        ##e.g. utils.substr_after("Carlson School","Carlson") will give you " School".
        
        sch = tr.find("a",class_="school-name")
        if sch is not None:
            data['href'] = utils.substr_after(sch.get('href'),"/best-graduate-schools/top-business-schools/")
            data['name'] = sch.string.strip()
        else:
            print "cannot locate school name"
            print tr
            quit()
        
        schools.append(data)
        
    return schools


# In[ ]:

# test the parse_page function here
pprint(parse_page(page2))


# ## the following function can request a range of pages one by one

# ##todo:
# Observe the following function, and fill in the todo blank as instructed
# 
# Test it in the next cell

# In[ ]:

def crawl_all_pages(start_page=1, total_pages=1):
    ''' crawl all pages in a multi-page environment
    Args:
        start_page: start from which page?
        total_pages: how many pages are there.
    '''
    schools =[] #to hold a list of schools.
    page = start_page #we use page as a counter
    
    while page <= total_pages:
        ## todo:
        ## you use the functions we have built to request and parse the page 
        ## and save the results to schools
        html = fetch_page(page)
        new_schools = parse_page(html)
        schools += new_schools #joining two lists
          
        utils.my_sleep() #pause between requests
        
        page+=1 #don't forget this, increase page to get the next page     
    
    return schools
        


# ## Now do the actual scraping

# In[ ]:

# to save time, we only crawl the first two pages
# it should give you 50 schools
schools = crawl_all_pages(1,2)


# In[ ]:

# print schools on screen
pprint(schools)


# In[ ]:

# save to a csv file to complete the whole cycle
utils.dump_csv(schools, "data/schoollist.csv")

